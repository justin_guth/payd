
# Payd

Payd is a small utility app that makes creating bills easier by using data from the Toggl API.

## Setting up

### Installing dependencies

Make sure you have "wkhtmltopdf" installed. You can find the latest version [here](https://wkhtmltopdf.org/downloads.html).   
Furthermore make sure you have installed the pdfkit, argparse and TogglPy packages:

`pip install TogglPy`   
`pip install pdfkit`  
`pip install argparse`

### Configuring the environment

In order for everything to work you need to set up your configuration file.

Some fields may need some explanation:

| field                 | meaning                                                                                |
| --------------------- | -------------------------------------------------------------------------------------- |
| `token`               | is the API token you are going to use to connect to Toggl.                             |
| `zero-fill-count`     | is the number of digits to which an integer will be filled (for example for bill ids). |
| `bill-id-pattern`     | The pattern used for the bill identifier. This feature is yet to improve.              |
| `render-name-pattern` | The file name of the resulting bill PDF.                                               |
| `template`            | The location of the template HTML.                                                     |
| `date-name-pattern`   | The pattern used for printing dates.                                                   |
| `date-range-pattern`  | The pattern used to print date ranges.                                                 |
| `entries-per-page`    | The number of rows that should be rendered per page.                                   |
| `company`             | This field lists information about your company.                                       |
| `clients`             | This field lists an array of your clients' information.                                |

The `company` fields should be pretty much self explanatory apart from the first:

| field        | meaning                                                                                                       |
| ------------ | ------------------------------------------------------------------------------------------------------------- |
| `company.id` | An id you want to assign. In the future this will allow to select the bill issuer. Currently without function |

The clients set within the `client` field are similar to the ones in the `company` field. The following fields exist in addition:

| field                            | meaning                                                                                                                                                                        |
| -------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `clients[n].id`                  | An id you want to assign.                                                                                                                                                      |
| `clients[n].hourly-rate`         | The rate at which you bill this client. The unit used is cents.                                                                                                                |
| `clients[n].hourly-rate-support` | The rate at which you bill this client for support work. Support work can be marked with "[Support]" in the Toggl time entry but is yet unimplemented. The unit used is cents. |
| `clients[n].rounding-rule`       | The rounding rule used to handle unfinished hours. Options are currently `up-full-hour` and `up-tenth-hour`. If needed more can be requested or implemented by oneself.        |
| `clients[n].toggl-cid`           | The client id assigned by Toggl for your client. This value can be found by running `py LoadCids.py`.                                                                          |

## Usage

You will need to call the Main.py file with the flags you want. First of all, the program pipeline needs a data source from either the Toggl API or a JSON file.
The Toggl and JSON input flags are mutually exclusive but required.

### Loading from Toggl

A Toggl source can be specified by providing the `-t` or `--toggl` flag. When this flag is set the program will access the data using the set Toggl API key in the config file.

When loading from Toggl you need to set the start and end of your date range. the `-s` or `--start` and `-e` or `--end` flags are used for this respectively. Those flags must be followed by an ISO formatted datetime (e.g. 2021-03-09).
The tool will read all entries from 00:00 on the start date to 23:59 on the end date.

### Loading from JSON

A JSON source can be specified by providing the `-i` or `--input` flag. This flag requires a following path to a json file containing entry data. How such a file can be created see below.


### Optional flags

| flag                            | parameters                                                                                                      | description                                                                                                                                                                         |
| ------------------------------- | --------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `-d / --dump`                   | Optional: The path where to dump the JSON to                                                                    | Dumps the retrieved entries into a JSON file. If no path is specified a default path pattern will be generated                                                                      |
| `-dh / --dumphtml`              | Optional: The path where to dump the rendered HTML to                                                           | Dumps the rendered HTML into a HTML file. If no path is specified a default path pattern will be generated. This feature is yet to be implemented                                   |
| `-l / --log`                    | Optional: The path where to dump the log to                                                                     | Dumps all entries from the input into a text based log listing all entries line by line. If no path is specified a default path pattern will be generated.                          |
| `-lg / --loggrouped`            | The client ids as specified in your configuration for which logs should be generated (e.g. `-lg 0 3 7`)         | Dumps all entries from the input into a text based log listing all entries line by line. One file will be created for each specified client containing entries filtered by clients. |
| `-r / --render`                 | The client ids as specified in your configuration for which rendered PDFs should be generated (e.g. `-r 0 3 7`) | Renders bills for all specified client ids.                                                                                                                                         |
| `-re / --renderempty`           | None                                                                                                            | Whether the app should render bills for clients without time entries.                                                                                                               |
| `-is / --ignoresupercategories` | None                                                                                                            | Whether the tool should not group entries into supercategories as explained below.                                                                                                  |
| `-ip / --ignorepattern`         | A regex pattern to ignore if found in the entry title (e.g. `-ip ".*Charity.*"`)                                | Remove all entries from working set matching the provided regex pattern in the title.                                                                                               |
| `-sp / --selectpattern`         | A regex pattern to use if found in the entry title (e.g. `-sp ".*Billable.*"`)                               | Select only entries from working set matching the provided regex pattern in the title.                                                                                              |
| `-tt / --title`                 | A string                                                                                                        | Specifies a title that will be put in the `title` template macro.                                                                                                                   |
| `-od / --overridedate`          | A date in ISO format (e.g. `-od 2021-03-09`)                                                                    | Specifies a date to be rendered in the bill instead of todays date                                                                                                                  |
| `-oi / --overrideid`            | A string                                                                                                        | Specifies an id to be used in the bills instead of the default generated one.                                                                                                       |

## Supercategories

Supercategories can be given to entries by writing the name of the category followed by a pipe ("`|`") symbol before the entry description. All categorised entries will be grouped and have their durations summed together.

## Templates 

Templates are filled with data and rendered into PDF files. An example template is provided in the `templates` directory. Templates allow the usage of some macros explained below:

| macro                          | function                                                                                                                       |
| ------------------------------ | ------------------------------------------------------------------------------------------------------------------------------ |
| `bill_id`                      | The bill generated id (or overridden one if specified). The macro is formatted as specified in the config at `bill-id-pattern` |
| `bill_time_range`              | The bill date range. The macro is formatted as specified in the config at `bill-id-pattern`                                    |
| `client_company_city`          | The client company city name specified in the config at `client[n].address.city-name`                                          |
| `client_company_contact_name`  | The client company contact name specified in the config at `client[n].address.contact-name`                                    |
| `client_company_house_number`  | The client company house number specified in the config at `client[n].address.house-number`                                    |
| `client_company_name`          | The client company name specified in the config at `client[n].name`                                                            |
| `client_company_street`        | The client company street name specified in the config at `client[n].address.street-name`                                      |
| `client_company_zip_code`      | The client company zip code specified in the config at `client[n].address.postal-code`                                         |
| `client_id`                    | The client id specified in the config at `client[n].address.id`                                                                |
| `company_address_city`         | The company address city name specified in the config at `company.address.city-name`                                           |
| `company_address_house_number` | The company address house number specified in the config at `company.address.house-number`                                     |
| `company_address_street`       | The company address street name specified in the config at `company.address.street-name`                                       |
| `company_address_zip_code`     | The company address zip code specified in the config at `company.address.postal-code`                                          |
| `company_bank_account_owner`   | The bank account owner name specified in the config at `company.bank-account.owner-name`                                       |
| `company_bank_bic`             | The bank account bic specified in the config at `company.bank-account.bic`                                                     |
| `company_bank_iban`            | The bank account iban specified in the config at `company.bank-account.iban`                                                   |
| `company_bank_name`            | The bank name specified in the config at `company.bank-account.bank-name`                                                      |
| `company_email`                | The company email specified in the config at `company.email`                                                                   |
| `company_name`                 | The company name specified in the config at `company.name`                                                                     |
| `company_phone`                | The company phone specified in the config at `company.phone-number`                                                            |
| `company_tax_id`               | The tax id as specified in the config at `company.tax-id`                                                                      |
| `company_website`              | The company website specified in the config at `company.website`                                                               |
| `date`                         | The current date (or overridden one if specified). The macro is formatted as specified in the config at `date-name-pattern`    |
| `formatted_table_data`         | An html table DOM string generated by the tool                                                                                 |
| `page`                         | The page number                                                                                                                |
| `price_sum`                    | The formatted sum for all the entries                                                                                          |
| `title`                        | The title as specified by the `--title / -tt` flag                                                                             |


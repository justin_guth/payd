from src.Config import Config
from src.TogglLoader import TogglLoader 
from src.FileLoader import FileLoader 
from src.EntryFilter import EntryFilter
from src.Serialiser import Serialiser
from src.Renderer import Renderer
import argparse
import datetime
import os

parser = argparse.ArgumentParser(description='Parse user options for payd')

def file_path(string):
    if os.path.isfile(string):
        return string
    else:
        raise argparse.ArgumentError(string + " is not a valid source path")

parser.add_argument(
    '--start', "-s",
    nargs = 1,
    dest = "start",
    metavar = 'YYYY-MM-DD',
    type = datetime.date.fromisoformat,
    help = 'The Start Date - format YYYY-MM-DD',
)

parser.add_argument(
    '--end', "-e",
    nargs = 1,
    dest = "end",
    metavar = 'YYYY-MM-DD',
    type = datetime.date.fromisoformat,
    help = 'The End Date - format YYYY-MM-DD',
)

sourceGroup = parser.add_mutually_exclusive_group(required=True)
patternGroup = parser.add_mutually_exclusive_group(required=False)


sourceGroup.add_argument(
    "-i", "--input",
    nargs = 1,
    dest = "source_file",
    default=False,
    metavar = 'path.json',
    type = file_path,
    help = 'The input file path'
)

sourceGroup.add_argument(
    "-t", "--toggl",
    nargs = "?",
    dest = "token",
    default = False,
    metavar = 'path.json',
    type = str,
    help = 'The input file path',
)


parser.add_argument(
    '--dump', "-d",
    nargs = "?",
    dest = "dump",
    default = False,
    metavar = 'path.json',
    type = str,
    help = 'Where to dump entries'
)

parser.add_argument(
    '--dumphtml', "-dh",
    nargs = "?",
    dest = "dumphtml",
    default = False,
    metavar = 'path.json',
    type = str,
    help = 'Where to dump intermediate html files'
)

parser.add_argument(
    '--log', "-l",
    nargs = "?",
    dest = "log",
    default = False,
    metavar = 'path.txt',
    type = str,
    help = 'Where to dump entries'
)

parser.add_argument(
    '--loggrouped', "-lg",
    nargs = "*",
    dest = "loggrouped",
    default = False,
    metavar = 'path.txt',
    type = str,
    help = 'Where to dump entries'
)

parser.add_argument(
    '--render', "-r",
    nargs = "*",
    dest = "render",
    default = False,
    metavar = '0 1 2',
    type = int,
    help = 'which client ids to render'
)

parser.add_argument(
    '--renderempty', "-re",
    nargs = 1,
    dest = "renderempty",
    default = False,
    metavar = 'true',
    type = bool,
    help = 'whether to render empty bills'
)

parser.add_argument(
    '--ignoresupercategories', "-is",
    nargs = 1,
    dest = "ignoresupercategories",
    default = False,
    metavar = 'true',
    type = bool,
    help = 'whether to ignore grouping entries into super categories'
)


patternGroup.add_argument(
    '--ignorepattern', "-ip",
    nargs = 1,
    dest = "ignorepattern",
    default = False,
    metavar = 'true',
    type = str,
    help = 'A regex pattern to ignore if the description matches'
)


patternGroup.add_argument(
    '--selectpattern', "-sp",
    nargs = 1,
    dest = "selectpattern",
    default = False,
    metavar = 'true',
    type = str,
    help = 'A regex pattern to select if the description matches'
)

parser.add_argument(
    '--title', "-tt",
    nargs = 1,
    dest = "title",
    default = False,
    metavar = 'true',
    type = str,
    help = 'A title to display in the bill'
)


parser.add_argument(
    '--overridedate', "-od",
    nargs = 1,
    dest = "overridedate",
    default = False,
    metavar = 'YYYY-MM-DD',
    type = datetime.date.fromisoformat,
    help = 'The bills Date - format YYYY-MM-DD',
)

parser.add_argument(
    '--overrideid', "-oi",
    nargs = 1,
    dest = "overrideid",
    metavar = '2020-12-15',
    default = False,
    type = str,
    help = 'The bills id',
)




args = parser.parse_args()
Config.init(args=args)

### Begin parsing

data = None

if (args.token != False):

    # TODO default behaviour if no start or end are specified

    data = TogglLoader.load(
        apiToken = Config.get("token"),
        start = args.start,
        end = args.end
    )

elif (args.source_file != False):

    data = FileLoader.load(
        args.source_file[0]
    )

if (args.ignorepattern != False):

    print("Ignoring pattern:", args.ignorepattern[0])
    data.filterOutPattern(args.ignorepattern[0])

if (args.selectpattern != False):

    print("Selecting pattern:", args.selectpattern[0])
    data.filterInPattern(args.selectpattern[0])



if (args.dump != False):

    destination = args.dump

    if destination == None:

        destination = "./dump/" + datetime.datetime.now().isoformat().replace(":", "-").split(".")[0] + ".json"

    Serialiser.serialiseEntryList(data, destination)



if (args.log != False):

    destination = args.log

    if destination == None:

        destination = "./log/" + datetime.datetime.now().isoformat().replace(":", "-").split(".")[0] + ".txt"

    Serialiser.logEntryList(data, destination)


groupedEntries = EntryFilter.group(data)

if (args.loggrouped != False):

    ids = args.loggrouped

    Serialiser.logGroupedEntryList(groupedEntries, ids)



mergedEntries = EntryFilter.mergeGrouped(groupedEntries)

if (args.render != False):

    ids = args.render

    print("Rendering ids:", ids)

    Renderer.render(mergedEntries, ids)

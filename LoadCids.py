from toggl.TogglPy import Toggl
from src.Config import Config

Config.init()

apiToken = Config.get("token")
toggl = Toggl()
toggl.setAPIKey(apiToken) 

clientInfo = toggl.request("https://api.track.toggl.com/api/v8/clients")

for x in list(
    map(
        lambda a : a["name"] + " -> " + str(a["id"]), 
        clientInfo
        )
    ):
    
    print(x) 

        
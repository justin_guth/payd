import re

class Entry:

    def __init__(self):

        self._data = {}

    @staticmethod
    def fromJson(jsonData):

        result = Entry()

        result._data["description"] = jsonData["description"]
        result._data["start"] = jsonData["start"]
        result._data["stop"] = jsonData["stop"]
        result._data["duration"] = jsonData["duration"]
        result._data["project-id"] = jsonData["pid"]
        result._data["client-id"] = jsonData["cid"]
        result._data["project-name"] = jsonData["project-name"]
        result._data["category"] = jsonData["description"].split("|")[0] if "|" in jsonData["description"] else ""
        result._data["title"] = jsonData["description"].split("|")[1] if "|" in jsonData["description"] else jsonData["description"]

        return result

    @staticmethod
    def fromJsonDump(jsonData):

        result = Entry()

        result._data["description"] = jsonData["description"]
        result._data["start"] = jsonData["start"]
        result._data["stop"] = jsonData["stop"]
        result._data["duration"] = jsonData["duration"]
        result._data["project-id"] = jsonData["project-id"]
        result._data["client-id"] = jsonData["client-id"]
        result._data["project-name"] = jsonData["project-name"]
        result._data["category"] = jsonData["category"]
        result._data["title"] = jsonData["title"]

        return result

    def __repr__(self):

        return str(self._data) 

    def description(self):

        return self._data["description"]

    def cleanDescription(self):

        return re.sub(r":\S*(\s|$)", "",self._data["description"])

    def isFree(self): 

        return ":free" in self.description()

    def clientId(self):

        return self._data["client-id"]

    def duration(self):

        return self._data["duration"]

    def category(self):

        return self._data["category"] if (self._data["category"] != "") else self.description()

    def start(self):

        return self._data["start"]

    def stop(self):

        return self._data["stop"]

    def title(self):

        return self._data["title"]

    def toJson(self):

        return self._data

    def reduction(self):

        return False
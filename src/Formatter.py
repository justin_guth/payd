from src.Config import Config
import datetime

class Formatter:

    @staticmethod
    def formatDate(datetimeObject):

        datePattern = Config.get("date-name-pattern")

        result = datePattern.format(
            **
            {
                "year": datetimeObject.year,
                "month": Formatter.zeroFillTo(2, datetimeObject.month),
                "day": Formatter.zeroFillTo(2, datetimeObject.day)
            }
        )

        return result

    @staticmethod
    def zeroFillTo(count, value):

        strValue = str(value)
        fillCount = count - len(strValue)

        return ("0" * fillCount) + strValue
from toggl.TogglPy import Toggl
import calendar
import datetime
from src.payd_utility import zfd

class TogglConnector():
    """description of class"""

    def __init__(self, apiToken, userAgent):

        self.toggl = Toggl()
        self.toggl.setAPIKey(apiToken) 

        

        return super().__init__()

    def getTimeEntries(self, month, year=datetime.datetime.now().year):

        if month < 1 or month > 12:

            raise IndexError("The month value is not between 1 and 12")

        monthDayRange = calendar.monthrange(year,month)

        firstDay = "01"
        lastDay = str(monthDayRange[1])


        lastDay = lastDay if len(lastDay) == 2 else "0" + str(lastDay)
        dateMonth = str(month) if len(str(month)) == 2 else "0" + str(month)


        data = {
            'start_date': str(year) + "-" + dateMonth + "-" + firstDay +"T00:00:00+02:00",
            'end_date': str(year) + "-" + dateMonth + "-" + lastDay +"T23:59:59+02:00"
        }  

        #print(data)

        response = self.toggl.request("https://api.track.toggl.com/api/v8/time_entries", parameters=data)

        return self.cleanUp(response)



    def getTimeEntriesForRange(self, fromDate, toDate, doDumpEntries=False):

        data = {
            'start_date': zfd(str(fromDate.year)) + "-" + zfd(str(fromDate.month)) + "-" + zfd(str(fromDate.day)) + "T00:00:00+02:00",
            'end_date': zfd(str(toDate.year)) + "-" + zfd(str(toDate.month)) + "-" + zfd(str(toDate.day)) + "T23:59:59+02:00"
        }
        print("request data:", data)

        response = self.toggl.request("https://api.track.toggl.com/api/v8/time_entries", parameters=data)

        cleanResponse = self.cleanUp(response)
        return cleanResponse

    def cleanUp(self, timeEntries):

        cleanData = []
        
        for entry in timeEntries:

            currentCleanEntry = {}

            #print ("toggl entry object:", entry)

            currentCleanEntry["description"] = entry["description"]
            currentCleanEntry["duration"] = entry["duration"]
            currentCleanEntry["start-moment"] = entry["start"]
            currentCleanEntry["end-moment"] = entry["stop"] if "stop" in entry.keys() else "1970-01-01T12:00:00+00:00"
            currentCleanEntry["pid"] = str(entry["pid"])
           
            cleanData.append(currentCleanEntry)

        #print("cleaned up entries:\n", cleanData)

        pidOrderedEntries = {}

        #pidOrderedDataDump = {}

        for entry in cleanData:

            pid = entry["pid"]

            if pid not in pidOrderedEntries.keys():


                projectInfo = self.toggl.request("https://api.track.toggl.com/api/v8/projects/" + pid)

                pidOrderedEntries[pid] = {
                        
                        "name" : projectInfo["data"]["name"],
                        "pid" : pid,
                        "cid" : str(projectInfo["data"]["cid"]),
                        "entries" : []

                }

                """pidOrderedDataDump[pid] = {

                    "pid" : pid,
                    "entries" : []
                }"""

            pidOrderedEntries[pid]["entries"].append(entry)
            #pidOrderedDataDump[pid]["entries"].append(entry)


        #print("pid ordered entries:\n", pidOrderedEntries)


        cidOrderedEntries = {}
        #cidOrderedDataDump = {}

        for pid in pidOrderedEntries.keys():

            cid = pidOrderedEntries[pid]["cid"]

            if cid not in cidOrderedEntries.keys():


                clientInfo = self.toggl.request("https://api.track.toggl.com/api/v8/clients/" + cid)

                cidOrderedEntries[cid] = {
                        
                        "name" : clientInfo["data"]["name"],
                        "projects" : [],
                        "cid": cid

                    }

            cidOrderedEntries[cid]["projects"].append(pidOrderedEntries[pid])



        #print("cid ordered entries:\n", cidOrderedEntries)


        return cidOrderedEntries




        """
        pidOrderedEntries = {}

        for entry in timeEntries:

            pid = str(entry["pid"])
            
            if entry["pid"] not in pidOrderedEntries.keys():

                pidOrderedEntries[pid] = [entry]
           
            else:

                pidOrderedEntries[pid].append(entry)
            

        print("Time entries ordered by project id:\n", pidOrderedEntries)

        cidOrderedEntries = {}

        for pid in pidOrderedEntries.keys():

            projectInfo = self.toggl.request("https://api.track.toggl.com/api/v8/projects/" + str(pid))
            cid = str(projectInfo["data"]["cid"])
            
            if int(cid) not in cidOrderedEntries.keys():

                newEntry = pidOrderedEntries[pid]
                #newEntry["project_name"] = projectInfo["data"]["name"]
                cidOrderedEntries[cid][str(pid)] = [newEntry]

                #clientInfo = self.toggl.request("https://api.track.toggl.com/api/v8/clients/" + str(cid))
                #cidOrderedEntries[cid]["client_name"] = clientInfo["name"]
           
            else:

                newEntry = pidOrderedEntries[str(pid)]
                newEntry["project_name"] = projectInfo["name"]
                cidOrderedEntries[projectInfo["cid"]][str(pid)].append(newEntry)
            

        print("Time entries ordered by client id:\n", cidOrderedEntries)
        """


        
from src.GroupedEntryList import GroupedEntryList
import re

class SplitGroupedEntryList:

    def __init__(self, splits):

        self._list = [{"list":GroupedEntryList(), "title":split["title"] } for split in splits]
        self._splits = splits

    def merge(self, entry):

        description = entry.description()
        splitIndex = None


        index = 0
        
        for split in self._splits:

            if split["patternType"] == "select" and re.match(split["pattern"], description) \
                or split["patternType"] == "ignore" and not re.match(split["pattern"], description):

                splitIndex = index

            index += 1

        if splitIndex == None:

            return


        self._list[splitIndex]["list"].merge(entry)

    def __repr__(self):

        return str(self._list)

    def toList(self):

        return self._list[:]
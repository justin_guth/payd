def truncate(n, decimals):
    return int(n * (10 ** decimals)) /  (10 ** decimals)


class RuleUpFullHour():

    def __init__(self):

        pass

    def toString(self):

        return "up-full-hour"

    def round(self, seconds):

        if seconds % (60 * 60) == 0:

            return seconds // (60 * 60) #return exact number of hours

        else:

            return seconds // (60 * 60) + 1 #return rounded number of hours if number of seconds is not exactly one hour

    def format(self, seconds):

        return str(self.round(seconds))

class RuleUpTenthHour():

    def __init__(self):

        pass

    def toString(self):

        return "up-tenth-hour"

    def round(self, seconds):

        if seconds % (60 * 6) == 0:

            return truncate(seconds / (60 * 60), 1)

        else:

            return truncate( float(seconds) / (60 * 60), 1) + 0.1

    def format(self, seconds):

        return str(f'{self.round(seconds):.1f}').replace(".", ",")

def createFromString(name):

    rules = [RuleUpFullHour(), RuleUpTenthHour()]

    for rule in rules:

        if rule.toString() == name:

            return rule

    return None
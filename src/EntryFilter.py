from src.Config import Config
from src.EntryList import EntryList
from src.GroupedEntryList import GroupedEntryList
from src.SplitGroupedEntryList import SplitGroupedEntryList


def select_cid_and_workspace_id(clientConfig):

    splits = [{
        "patternType": "select",
        "pattern": ".*",
        "name": "default",
        "title": None
    }]


    if "splits" in clientConfig.keys():

        splits = clientConfig["splits"]

    return {
        "local-id": clientConfig["id"],
        "cid": clientConfig["toggl-cid"],
        "entries": EntryList(),
        "grouped-entries": SplitGroupedEntryList(splits)
    }


class EntryFilter:

    @staticmethod
    def group(entryList):
        """
        Groups the entries by client
        """

        clients = {
            c["cid"]: c for c in list(map(select_cid_and_workspace_id, Config.get("clients")))
        }

        for entry in entryList.toList():

            cid = entry.clientId()

            if cid not in clients.keys():

                print(cid, "not found in client configuration. Skipping...")
                continue

            clients[cid]["entries"].push_back(entry)

        return clients

    @staticmethod
    def mergeGrouped(groupedEntries):
        """
        Merges the entries by category but respects splits in the configutration
        """

        for cid in groupedEntries.keys():

            confiClient = list(
                filter(lambda c: c["toggl-cid"] == cid, Config.get("clients")))

            if confiClient == []:

                continue

            for entry in groupedEntries[cid]["entries"].toList():

                groupedEntries[cid]["grouped-entries"].merge(entry)

        return groupedEntries

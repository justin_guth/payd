from src.GroupedEntry import GroupedEntry
import re

class GroupedEntryList:

    def __init__(self):

        self._list = {}

    def merge(self, entry):

        category = entry.category()

        if category not in self._list.keys():

            self._list[category] = GroupedEntry.fromEntry(entry)

        else:

            self._list[category].merge(entry)

    def __repr__(self):

        return str(self._list)

    def toList(self):

        return self._list.values()
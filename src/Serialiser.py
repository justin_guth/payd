import json
import codecs
from src.EntryList import EntryList
import datetime

class Serialiser:

    @staticmethod 
    def serialiseEntryList(entryList, destination):

        with open(destination, "w") as f:

            f.write(Serialiser.getJsonSerialisationString(entryList))

    @staticmethod 
    def logEntryList(entryList, destination):

        with codecs.open(destination, "w", "utf-8") as f:

            f.write(Serialiser.getLogString(entryList))

    @staticmethod
    def logGroupedEntryList(groupedEntries, ids):


        for client in groupedEntries:

            if ids == [] or groupedEntries[client]["local-id"] in ids:

                Serialiser.logEntryList(
                    groupedEntries[client]["entries"],
                    destination = "./grouped logs/" + datetime.datetime.now().isoformat().replace(":", "-").split(".")[0] + "_" + groupedEntries[client]["local-id"] + ".txt"
                )


    @staticmethod
    def getLogString(entryList):

        result = ""

        for entry in entryList.toList():

            result += "[" + entry.start().split("+")[0] + " - " + entry.stop().split("+")[0] + "]"
            result += " " + entry.description() + "\n"

        return result

    @staticmethod
    def getJsonSerialisationString(entryList):

        jsonString = json.dumps(entryList.toJson()) 

        result = ""
        depth = 0
        inString = False
        spacesPerDepth = 4

        for character in jsonString:

            if not inString and character == " ":

                continue

            if character == "\"":

                inString = not inString
                result += "\""

            elif character == "{":

                depth += 1
                result += "{\n" + " " * depth * spacesPerDepth 

            elif character == "[":

                depth += 1
                result += "[\n" + " " * depth * spacesPerDepth 

            elif character == "]":

                depth -= 1
                result += "\n" + " " * (depth) * spacesPerDepth + "]" 

            elif character == "}":

                depth -= 1
                result += "\n" + " " * (depth) * spacesPerDepth + "}"
            
            elif character == ",":

                result += ",\n" + " " * depth * spacesPerDepth 

            elif character == ":" and not inString:

                result += ": "

            else:

                result += character

        return result
import re

class GroupedEntry:

    def __init__(self):

        self._data = {}

    @staticmethod
    def fromEntry(entry):

        # TODO add reduction check for entry

        result = GroupedEntry()

        result._data["duration"] = entry.duration()
        result._data["category"] = entry.category()

        return result

    def merge(self, entry):

        if self._data["category"] != entry.category():

            raise Exception("incompatible category for merging!")

        self._data["duration"] += entry.duration()
        

    def __repr__(self):

        return str(self._data) 

    def duration(self):

        return self._data["duration"]

    def category(self):

        return self._data["category"]

    def cleanCategory(self):

        return re.sub(r":\S*(\s|$)", "", self._data["category"])

    def reduction(self):

        return False

    def isFree(self): 

        return (":free" in self.category())

from src.Entry import Entry
import json
import re

class EntryList:

    def __init__(self):

        self._list = []

    def push_back(self, entry):

        self._list.append(entry)

    def __repr__(self):

        return str(self._list)

    @staticmethod
    def fromToggl(jsonResponse):

        result = EntryList()

        for entry in jsonResponse:

            result.push_back(Entry.fromJson(entry)) 

        return result

    @staticmethod
    def fromJson(jsonData):

        result = EntryList()

        for entry in jsonData:

            result.push_back(Entry.fromJsonDump(entry)) 

        return result

    def toList(self):

        return self._list

    def toJson(self):

        return [x.toJson() for x in self._list]

    def filterOutPattern(self, pattern):

        newList = []

        for entry in self._list:

            if re.match(pattern, entry.description()) == None:

                newList.append(entry)

        self._list = newList

    def filterInPattern(self, pattern):

        newList = []

        for entry in self._list:

            if re.match(pattern, entry.description()) != None:

                newList.append(entry)

        self._list = newList
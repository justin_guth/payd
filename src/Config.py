import json
import codecs

class Config:

    _args = None

    @staticmethod
    def init(path = "./config.json", args = None):

        with codecs.open(path, "r", "utf-8") as f:

            Config._data = json.loads(f.read())

        Config.setArgs(args)

    @staticmethod
    def data():

        return Config._data

    @staticmethod
    def args():

        return Config._args

    @staticmethod
    def setArgs(args):

        Config._args = args

    @staticmethod
    def get(key):

        return Config.data()[key]


    @staticmethod
    def getClientInfo(id):

        for client in Config.get("clients"):

            if client["id"] == id:

                return client


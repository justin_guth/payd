from src.EntryList import EntryList
from toggl.TogglPy import Toggl

class TogglLoader:

    def __init__(self):

        pass

    @staticmethod
    def load(apiToken, start, end, timezone = "+02:00"):

        toggl = Toggl()
        toggl.setAPIKey(apiToken)

        if (start != None and end == None) or (start == None and end != None):

            print("Either both or neither start nor end must be provided!")
            exit()

        startString = start[0].isoformat()
        endString = end[0].isoformat()


        data = {
            'start_date': startString + "T00:00:00" + timezone,
            'end_date': endString + "T23:59:59" + timezone
        }

        response = toggl.request("https://api.track.toggl.com/api/v8/time_entries", parameters=data)

        projectInfos = {}

        for i in range(len(response)):

            pid = str(response[i]["pid"]) 
            response[i]["pid"] = pid # set to string parsed value

            if pid not in projectInfos.keys():

                projectInfo = toggl.request("https://api.track.toggl.com/api/v8/projects/" + pid)
                projectInfos[pid] = {
                    "name": projectInfo["data"]["name"],
                    "cid": str(projectInfo["data"]["cid"])
                } 

            response[i]["project-name"] = projectInfos[pid]["name"]
            response[i]["cid"] = projectInfos[pid]["cid"]

        return EntryList.fromToggl(response)

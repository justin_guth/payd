from src.Config import Config
from src.Formatter import Formatter
import src.RoundingRules as RoundingRules
import datetime
import html
import codecs
import pdfkit


class Renderer:

    @staticmethod
    def render(mergedEntries, ids):

        counter = 0

        for client in mergedEntries:

            if ids == [] or int(mergedEntries[client]["local-id"]) in ids:

                for splitGroup in mergedEntries[client]["grouped-entries"].toList():

                    if len(splitGroup["list"].toList()) == 0 and Config.args().renderempty == False:

                        continue

                    billId = Renderer.getBillId(mergedEntries[client], counter=counter)

                    if (Config.args().overrideid != False):

                        billId = Config.args().overrideid[0]

                    dest = Renderer.getDestination(billId)

                    result = Renderer.getRenderedBill(mergedEntries[client], billId, splitGroup)

                    Renderer.writeRendered(result, dest)

                    counter += 1

    @staticmethod
    def writeRendered(result, dest):

        with codecs.open("./templates/tmp/tmp.html", "w", "utf-8") as f:

            f.write(result)
        
        options = {
            'page-size': 'A4',
            'margin-top': '0.75in',
            'margin-right': '0.75in',
            'margin-bottom': '0.75in',
            'margin-left': '0.75in',
        }


        pdfkit.from_file('./templates/tmp/tmp.html', dest, options=options) # TODO add pages

    @staticmethod
    def getBillId(client, counter = 0):

        now = datetime.datetime.now()

        result = Config.get("bill-id-pattern")
        return result.format(**
            {
                "year": now.year,
                "month": Formatter.zeroFillTo(2, now.month),
                "day": Formatter.zeroFillTo(2, now.day),
                "c": Renderer.zeroFillId(counter)
            }
        )

    @staticmethod
    def getRenderedBill(client, billId, splitGroup, page=1):

        templatePath = Config.get("template")

        company = Config.get("company")

        clientInfo = Config.getClientInfo(client["local-id"])

        dateRangePattern = Config.get("date-range-pattern")

        startTime = Formatter.formatDate(Config.args().start[0])
        endTime = Formatter.formatDate(Config.args().end[0])

        title = ""

        if Config.args().title != False:

            title = Config.args().title[0]

        elif splitGroup["title"] not in ["", None]:

            title =  splitGroup["title"]

        dateRangeString = dateRangePattern.format(
            ** {
                'from': startTime,
                'to': endTime 
            }
        )

        settings = {

            'company_name':html.escape(company["name"]),

            'company_address_street':html.escape(company["address"]["street-name"]),
            'company_address_house_number':html.escape(company["address"]["house-number"]),
            'company_address_zip_code':html.escape(company["address"]["postal-code"]),
            'company_address_city':html.escape(company["address"]["city-name"]),

            'company_website':html.escape(company["website"]),
            'company_email':html.escape(company["email"]),
            'company_phone':html.escape(company["phone-number"]),

            'company_bank_iban':html.escape(company["bank-account"]["iban"]),
            'company_bank_bic':html.escape(company["bank-account"]["bic"]),
            'company_bank_name':html.escape(company["bank-account"]["bank-name"]),
            'company_bank_account_owner': html.escape(company["bank-account"]["owner-name"]),
            
            'company_tax_id':html.escape(company["tax-id"]),

            'client_company_name':html.escape(clientInfo["name"]),
            'client_company_contact_name':html.escape(clientInfo["address"]["contact-name"]),

            'client_company_street':html.escape(clientInfo["address"]["street-name"]),
            'client_company_house_number':html.escape(clientInfo["address"]["house-number"]),
            'client_company_zip_code':html.escape(clientInfo["address"]["postal-code"]),
            'client_company_city':html.escape(clientInfo["address"]["city-name"]),



            'date':html.escape(Renderer.getDateString()),
            'client_id':html.escape(client["local-id"]),
            'bill_id':html.escape(billId),


            'formatted_table_data':Renderer.getFormattedDataTable(splitGroup["list"], clientInfo),
            'price_sum':html.escape(Renderer.formatEuro(Renderer.getTotalSum(client, clientInfo, splitGroup["list"]))),

            'page': page,

            'bill_time_range': html.escape(dateRangeString),
            'title': html.escape(title)
        }

        templateString = None

        with codecs.open(templatePath, "r", "utf-8") as f:

            templateString = f.read()

        formattedTable = templateString.format(**settings)

        return formattedTable

    @staticmethod
    def getDestination(billId):

        result = Config.get("render-name-pattern")
        return result.format(**
            {
                "bill-id": billId
            }
        )

    @staticmethod
    def formatEuro(cents):

        cents = str(round(cents))

        if len(cents) >= 3:

            return cents[:-2] + "," + cents[-2:]

        if len(cents) == 2:

            return "0," + cents

        if len(cents) == 1:

            return "0,0" + cents

        if len(cents) == 0 or cents == 0:

            return "0,00"

    @staticmethod
    def getTotalSum(client, clientInfo, splitGroup):

        sumVal = 0

        for entry in splitGroup.toList():

            if (entry.isFree()): continue
            sumVal += Renderer.getUnitCost(clientInfo) * Renderer.getUnits(entry.duration(), clientInfo)

        return sumVal

    @staticmethod
    def getUnitCost(client):

        # TODO account for reductions and for different pricings
        
        return client["hourly-rate"]

    @staticmethod
    def getUnits(duration, client):

        roundingRule = RoundingRules.createFromString(client["rounding-rule"])
        
        return roundingRule.round(duration)

    @staticmethod
    def getUnitsString(duration, client):

        roundingRule = RoundingRules.createFromString(client["rounding-rule"])
        
        return roundingRule.format(duration)


    @staticmethod
    def getFormattedDataTable(groupedEntries, client):

        formattedTable = ""

        for entry in groupedEntries.toList():

            formattedTable += "<tr>" 
            formattedTable += "<td>" + html.escape(entry.cleanCategory()) + "</td>"
            formattedTable += "<td class=\"right\">" + Renderer.formatEuro(Renderer.getUnitCost(client) if not entry.isFree() else 0) + "€</td>"
            formattedTable += "<td class=\"right\">" + Renderer.getUnitsString(entry.duration(), client) + "</td>"
            formattedTable += "<td class=\"right\">" + Renderer.formatEuro(Renderer.getUnitCost(client) * float(str(f'{Renderer.getUnits(entry.duration(), client):.1f}')) if not entry.isFree() else 0 ) +"€</td>"
            formattedTable += "</tr>"

        return formattedTable


    @staticmethod
    def getDateString():

        if (Config.args().overridedate != False):

            return Formatter.formatDate(Config.args().overridedate[0])

        now = datetime.datetime.now()
        return Formatter.formatDate(now)


    @staticmethod
    def zeroFillId(counter):

        return Formatter.zeroFillTo(Config.get("zero-fill-count"), counter)

    
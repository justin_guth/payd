import json
from src.EntryList import EntryList


class FileLoader:

    @staticmethod
    def load(path):

        with open(path, "r") as f:

            return EntryList.fromJson(json.loads(f.read()))